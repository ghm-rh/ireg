/*
 * Inode Registration Daemon
 *
 * Copyright 2018 Red Hat, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include <err.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/epoll.h>
#include <sys/mman.h>
#include <sys/file.h>

#include "ireg.h"

struct inode {
	struct inode *next;
	struct inode *prev;
	uint64_t ino;
	uint64_t dev;
	uint64_t version_offset;
	int64_t count;
};

struct inode_ref {
	struct inode_ref *next;
	struct inode_ref *prev;
	struct inode *inode;
};

struct client {
	int fd;
	unsigned int id;
	struct inode_ref list;
};

struct inode inode_list = {
	.next = &inode_list,
	.prev = &inode_list,
};

static int64_t *version_table;
static uint64_t first_free_version = 1;
static uint64_t version_table_size = 1 << 26;
static unsigned int num_clients = 0;
static unsigned int client_id_ctr = 1;

static void deb(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	printf("\n");
}

static void set_version(uint64_t version_offset, int64_t ver)
{
	__atomic_store_8(&version_table[version_offset], ver, __ATOMIC_SEQ_CST);
}

static struct inode *get_inode(uint64_t ino, uint64_t dev)
{
	struct inode *i;
	uint64_t next_free_version;
	struct inode *next, *prev;

	for (i = inode_list.next; i != &inode_list; i = i->next) {
		if (i->ino == ino && i->dev == dev) {
			i->count++;
			return i;
		}
	}

	/* Version table full? */
	if (first_free_version + 1 == version_table_size)
		return NULL;

	i = (struct inode *) calloc(1, sizeof(struct inode));
	if (!i)
		return NULL;

	i->ino = ino;
	i->dev = dev;
	i->version_offset = first_free_version;
	next_free_version = version_table[first_free_version];
	set_version(i->version_offset, 1);
	i->count = 1;

	prev = inode_list.prev;
	next = &inode_list;

	i->next = next;
	i->prev = prev;
	prev->next = i;
	next->prev = i;

	if (next_free_version >= version_table_size)
		errx(1, "corrupted free list in version table");
	first_free_version = next_free_version ?: first_free_version + 1;

	return i;
}

static void put_inode(struct inode *inode)
{
	struct inode *next, *prev;

	if (inode->count > 1) {
		inode->count--;
		return;
	}

	prev = inode->prev;
	next = inode->next;
	prev->next = next;
	next->prev = prev;

	version_table[inode->version_offset] = first_free_version;
	first_free_version = inode->version_offset;

	free(inode);
}

static void process_get_inode(struct client *c, struct ireg_msg *msg)
{
	int ret;
	struct inode_ref *iref =
		(struct inode_ref *) calloc(1, sizeof(struct inode_ref));
	struct inode *inode = iref ? get_inode(msg->get.ino, msg->get.dev) : NULL;
	struct srv_msg reply = {
		.op = SRV_VERSION,
		.handle = msg->handle,
		.version.offset = inode ? inode->version_offset : 0,
		.version.refid = inode ? (uintptr_t) iref : 0,
	};

	deb("[%i] GET INODE h=%llu i=%llu d=%llu -> v=%llu r=%llu",
	    c->id, (unsigned long long) msg->handle,
	    (unsigned long long) msg->get.ino,
	    (unsigned long long) msg->get.dev,
	    (unsigned long long) reply.version.offset,
	    (unsigned long long) reply.version.refid);

	ret = write(c->fd, &reply, sizeof(reply));
	if (ret != sizeof(reply)) {
		if (ret == -1)
			warn("[%i] write", c->id);
		else
			warnx("[%i] short write: %i", c->id, ret);
		put_inode(inode);
		inode = NULL;
	}

	if (inode) {
		struct inode_ref *prev, *next;

		iref->inode = inode;

		prev = c->list.prev;
		next = &c->list;

		iref->next = next;
		iref->prev = prev;
		prev->next = iref;
		next->prev = iref;

	} else {
		free(iref);
	}
}

static void free_iref(struct inode_ref *iref)
{
	struct inode_ref *prev, *next;

	put_inode(iref->inode);

	prev = iref->prev;
	next = iref->next;
	prev->next = next;
	next->prev = prev;

	free(iref);
}

static void process_put_inode(struct client *c, struct ireg_msg *msg)
{
	deb("[%i] PUT INODE r=%llu", c->id,
	    (unsigned long long) msg->put.refid);

	free_iref((struct inode_ref *) msg->put.refid);
}

static void free_client(struct client *c)
{
	deb("[%i] closed connection", c->id);

	while (c->list.next != &c->list)
		free_iref(c->list.next);

	close(c->fd);
	free(c);
}

static int serve_client(struct client *c)
{
	char buf[100];
	struct ireg_msg msg;
	int ret;

	ret = read(c->fd, buf, sizeof(buf));
	if (ret == -1) {
		warn("[%i] read", c->id);
		return 0;
	}

	if (ret == 0) {
		free_client(c);
		if (num_clients == 1)
			return 1;
		num_clients--;
	}

	if (ret != sizeof(struct ireg_msg)) {
		warnx("[%i] bad size message: %i", c->id, ret);
		return 0;
	}

	memcpy(&msg, buf, sizeof(msg));

	switch (msg.op) {
	case IREG_GET:
		process_get_inode(c, &msg);
		break;

	case IREG_PUT:
		process_put_inode(c, &msg);
		break;

	default:
		warnx("[%i] bad message type: %u", c->id, msg.op);
	}

	return 0;
}

static void new_client(int connection_socket, int epollfd)
{
	int data_socket;
	struct epoll_event ev;
	struct client *client;
	int ret;

	data_socket = accept(connection_socket, NULL, NULL);
	if (data_socket == -1) {
		warn("accept");
		return;
	}

	client = (struct client *) calloc(1, sizeof(struct client));
	if (!client)
		errx(1, "malloc");

	client->fd = data_socket;
	client->id = client_id_ctr++;
	client->list.prev = &client->list;
	client->list.next = &client->list;
	num_clients++;

	deb("[%i] new connection", client->id);

	ev.events = EPOLLIN;
	ev.data.ptr = client;
	ret = epoll_ctl(epollfd, EPOLL_CTL_ADD, data_socket, &ev);
	if (ret == -1)
		err(1, "epoll_ctl: data_socket");
}

int main(void)
{
	struct sockaddr_un name = { .sun_family = AF_UNIX };
	int ret;
	int connection_socket;
	int epollfd;
	const char *socket_name = "/tmp/ireg.sock";
#define MAX_EVENTS 10
	struct epoll_event ev, events[MAX_EVENTS];
	int n, nfds;
	const char *version_path = "/dev/shm/fuse_shared_versions";
	off_t filesize = version_table_size * sizeof(version_table[0]);
	void *addr;
	int fd;

	fd = open(version_path, O_RDWR | O_CREAT, 0600);
	if (fd == -1)
		err(1, "open: %s", version_path);

	/* only one instance can win this lock */
	ret = flock(fd, LOCK_EX | LOCK_NB);
	if (ret == -1) {
		deb("another instance already running: exiting");
		return 0;
	}

	ret = ftruncate(fd, 0);
	if (ret == -1)
		err(1, "ftruncate size: 0");

	ret = ftruncate(fd, filesize);
	if (ret == -1)
		err(1, "ftruncate size: %lli", (long long) filesize);

	addr = mmap(NULL, filesize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED)
		err(1, "mmap");

	close(fd);

	version_table = (int64_t *) addr;
	set_version(0, VERSION_TABLE_MAGIC);

	unlink(socket_name);
	connection_socket = socket(AF_UNIX, SOCK_SEQPACKET, 0);
	if (connection_socket == -1)
		err(1, "socket");

	strncpy(name.sun_path, socket_name, sizeof(name.sun_path) - 1);

	ret = bind(connection_socket, (const struct sockaddr *) &name,
		   sizeof(struct sockaddr_un));
	if (ret == -1)
		err(1, "bind");

	ret = listen(connection_socket, 10);
	if (ret == -1)
		err(1, "listen");

	deb("listening on %s", socket_name);

	epollfd = epoll_create1(0);
	if (epollfd == -1)
		err(1, "epoll_create1");

	ev.events = EPOLLIN;
	ev.data.ptr = NULL;
	ret = epoll_ctl(epollfd, EPOLL_CTL_ADD, connection_socket, &ev);
	if (ret == -1)
		err(1, "epoll_ctl: connection_socket");

	for (;;) {
		nfds = epoll_wait(epollfd, events, MAX_EVENTS, -1);
		if (nfds == -1)
			err(1, "epoll_wait");

		for (n = 0; n < nfds; n++) {
			if (events[n].data.ptr == NULL) {
				new_client(connection_socket, epollfd);
			} else {
				ret = serve_client((struct client *) events[n].data.ptr);
				if (ret) {
					unlink(version_path);
					deb("last connection closed: exiting");
					return 0;
				}
			}
               }
	}
}
